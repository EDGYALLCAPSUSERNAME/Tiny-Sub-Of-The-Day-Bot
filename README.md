Tiny Sub of the Day Bot
=======================

OAuth Configuration
--------------------

This bot requires python3, praw, [dataset](https://dataset.readthedocs.org/en/latest/), and the [prawoauthutil-2](https://github.com/SmBe19/praw-OAuth2Util) built by [/u/SmBe19](https://www.reddit.com/user/SmBe19).

To get this setup install praw, dataset, and praw-oauth2util:

    pip install praw
    pip install dataset
    pip install praw-oauth2util

Then follow the instructions for setting up a reddit app [here](https://github.com/SmBe19/praw-OAuth2Util/blob/master/OAuth2Util/README.md#reddit-config).

**The bot account must be a mod of both subs**

And then add your app_key and app_secret to the oauth.txt file.
