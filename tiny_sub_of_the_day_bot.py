import re
import sys
import time
import math
import praw
import string
import traceback
import logging
import requests
import itertools
import dataset
import OAuth2Util
from datetime import timedelta
from messages_template import *

PUBLIC_SUB = 'TinySubredditoftheDay'
PRIVATE_SUB = 'TSROTD_Dev'
EXCLUDE = set(string.punctuation)
# Remove the / and _ from the punctuation set because
# they are valid subreddit characters
EXCLUDE.remove('/')
EXCLUDE.remove('_')

# Configure logging
logging.basicConfig(filename='TSOTD.log',
                    level=logging.DEBUG,
                    format='%(asctime)s %(message)s')


def send_error_message_to_mods(subject, info, err, r):
    r.send_message('EDGYALLCAPSUSERNAME',
                   subject,
                   ('There was an error with ' +
                    '/r/{} \n\n'.format(info) +
                    'Err was: ```\n\n{}\n```'.format(err)),
                   captcha=None)


def message_mods(r, db, subject, subm=None):
    if subject == 'Warning: no post for today':
        s = ''
        # find the responses recieved posts
        submissions = r.search('flair:RECIEVED', 'TSROTD_Dev')
        for submission in submissions:
            s += '[{}]({})\n\n'.format(submission.title, submission.permalink)

        body = WARNING_MESSAGE.format(time.strftime("%B/%-d/%Y"), s)
    elif subject == 'Success: Subreddit of the day has been posted':
        body = SUCCESS_MESSAGE.format(time.strftime("%B/%-d/%Y"),
                                      subm.permalink)

    print('Messaging mods...')
    r.send_message('/r/TSROTD_Dev', subject, body, captcha=None)

    # Then add the date so it doesn't send multiple
    data = dict(date=time.strftime("%B/%-d/%Y"))
    db['warning'].insert(data)


def find_posts_to_xpost(public_sub, private_sub, r, db):
    warning = True
    print("Finding any posts...")
    submissions = private_sub.get_new(limit=100)
    for submission in submissions:
        if submission.link_flair_text == "READY FOR BOT POST":
            # Check if it's ready to be posted
            print("Checking if it's time to post...")
            sub_name = check_if_time_to_post(r, db, submission)
            # If the sub_name exists and is not False
            if sub_name is 'today':
                warning = False
            if sub_name and sub_name is not 'today':
                # Post to /r/TinySuboftheDay then flair dev post
                print("Setting flair...")
                submission.set_flair(flair_css_class="URGENT",
                                     flair_text="ALREADY FEATURED")
                print("Cross posting...")
                xpost_submission(submission, public_sub, r, sub_name, db)

    # check if warning was set to False, which would mean there is a post
    # ready for the day if there isn't warning will remain true
    # then check if it has already sent the warning message for the day
    # which is stored in a warning table as a string of the days date
    today = time.strftime("%B/%-d/%Y")
    if warning and db['warning'].find_one(date=today) == None:
        message_mods(r, db, 'Warning: no post for today')


def check_if_time_to_post(r, db, submission):
    # Make sure it's not a unicode object
    title = str(submission.title)
    # Remove any punctuation
    title = ''.join(ch for ch in title if ch not in EXCLUDE)
    # Split it into a list
    l = title.split(" ")
    try:
        # In case it is titled 1st or 1, 15th or 15, etc
        day = ''.join(re.findall('\d+', l[1]))
        # Extract the date from the list (Month/Day/Year)
        date = "{}/{}/{}".format(l[0], day, l[2])
        # Get the subreddit name
        sub_name = l[3][3:]
    except IndexError:
        logging.debug("ERROR: Can't read post title on post " +
                      "{}\n\nTitle is: {}".format(submission.permalink,
                                                  submission.title))
        submission.add_comment('You tried to kill me! I don\'t know ' +
                               'what to do with this.')
        submission.set_flair(flair_css_class='UNDECIDED',
                             flair_text="BOT CAN'T READ TITLE")
        return False

    # Gets the current hour and casts it into an int for comparison
    current_hour = int(time.strftime("%H"))
    # If the date is equal to today's date and it is 1 in the morning
    # Return the sub name
    # Note the '%-d' strips the leading zero, but will
    # only work on Linux/OS X and not on Windows
    today = time.strftime("%B/%-d/%Y")
    # this is the quick and dirty fix to setup warning messages
    # to be sent that worked with the current logic
    # on the to do list to rework into something less atrocious
    if date == today and current_hour >= 10:
        return sub_name
    elif date == today:
        return 'today'
    else:
        return False


def xpost_submission(submission, public_sub, r, sub_name, db):
    title = submission.title
    body = submission.selftext

    print("Submitting new post...")
    sub = public_sub.submit(title, text=body)

    # remove the old sticky before approving and stickying
    # the new one waiting for praw to update for the
    # two stickies
    public_sub.get_sticky().unsticky()
    # approve and sticky
    sub.approve()
    sub.sticky()
    # then let the winning sub know
    notify_sub(r, sub_name, sub)
    # then message the mods with success
    message_mods(r, db, 'Success: Subreddit of the day has been posted', sub)


def notify_sub(r, sub_name, submission):
    # set the feature title
    title = FEATURE_TITLE.format(sub_name)

    print("Posting to sub of the day...")

    # Tries to get the subreddit to post to
    try:
        sub = r.get_subreddit(sub_name)
        # This will cause an exception if it's a bad sub
        sub.title
    # In case winners sub goes private, exits function
    except praw.errors.Forbidden as e:
        print("ERROR: Forbidden Error")
        logging.debug("ERROR: Forbidden. Subname {} in ".format(sub_name) +
                      "notify_sub")
        send_error_message_to_mods('Winners sub is private',
                                   sub_name, e, r)
        return
    # In case the subreddit is invalid for some reason, exits function
    except praw.errors.InvalidSubreddit as e:
        print("ERROR: Invalid Subreddit")
        logging.debug("ERROR: InvalidSub. Subname {} in ".format(sub_name) +
                      "notify_sub")
        send_error_message_to_mods("Couldn't find winners sub",
                                   sub_name, e, r)
        return
    # Catch any/all weird errors and exit function
    except Exception as e:
        print("ERROR: {}".format(e))
        logging.debug("ERROR: Exception {}".format(e) +
                      " in notify_sub")
        etype, value, tb = sys.exc_info()
        tb_s = ''.join(traceback.format_exception(etype, value, tb, None))
        send_error_message_to_mods('Python error with winners sub',
                                   sub_name, tb_s, r)
        return

    # Tries to submit to subreddit
    try:
        sub.submit(title, url=str(submission.permalink))
    # Catch any weird errors because it couldn't submit
    # i.e sub is private/restricted
    except praw.errors.APIException as e:
        # If the issue is the sub doesn't allow links
        # Post a self text with link to subreddit of the day submission
        if e.error_type == 'NO_LINKS':
            sub.submit(title, text=SELF_TEXT_BODY.format(submission.permalink))
        else:
            print("ERROR: Praw API Exception {}".format(e))
            send_error_message_to_mods("Couldn't submit post to winners",
                                       sub_name, e, r)
    # And catch any other weird errors that may come up
    except Exception as e:
        print("ERROR: {}".format(e))
        send_error_message_to_mods('Python error with submission to winners',
                                   sub_name, e, r)


def get_nominations(public_sub, private_sub, db, r):
    # gets the replied to table from the db
    table = db['replied_to']
    mod_mail = public_sub.get_mod_mail(limit=50)

    print("Checking mod mail...")
    for mail in mod_mail:
        # Check to make sure it is a message object
        # For some reason was getting a str object
        # every now and then
        if type(mail) is not praw.objects.Message:
            continue
        # If it hasn't read it yet
        if table.find_one(mail_id=mail.id) is None:
            # And is a subreddit nomination
            if mail.subject == "Subreddit Nomination":
                print("Found a nomination...")
                # Find the info of the sub
                results = find_sub_nominations(mail, r)
                # If the info is good go ahead and make the post
                subs = make_nomination_post(results[0], private_sub)
                nom_message = generate_nominations_reply(subs)
                error_message = generate_errors_reply(results[1])

                # Only reply as long as one of them has a value
                if nom_message or error_message:
                    msg = (nom_message + error_message + BOT_FOOTER)

                    print("Replying to message...")
                    mail.reply(msg)
                else:
                    msg = """I couldn't find any subreddits
                    to nominate in your message please submit another
                    message with your including your nomination""" + BOT_FOOTER

                    print("Replying to message...")
                    mail.reply(msg)

        # Finally add the message id to the already read db
        data = dict(mail_id=str(mail.id))
        table.insert(data)


def find_sub_nominations(mail, r):
    # replace newlines with spaces
    mail_body = str(mail.body).replace('\n', ' ')
    # then split into a list
    l = mail_body.split(" ")
    subs = []
    errors = []
    for s in l:
        # Regex that will pull out the subname from the string
        search = re.search(r'(?<=r/)([a-zA-Z])\w+', s)
        if search:
            stats = determine_sub_stats(r, search.group(0), mail)
            # if it doesn't produce error, it will return a dict
            if type(stats) == dict:
                subs.append(stats)
            # if it does produce an error, it will return a list
            elif type(stats) == list:
                errors.append(stats)
            # then it ignores any other values because they are not
            # relevant

    # Remove any duplicates from both lists
    subs = [dict(t) for t in set(tuple(item.items()) for item in subs)]
    errors = list(errors for errors, _ in itertools.groupby(errors))
    return [subs, errors]


def determine_sub_stats(r, name, mail):
        # If they included TSOTD in the the nomination mail
        if 'tinysubredditoftheday' in name.lower():
            return False

        # Try to get the subreddit
        try:
            subreddit = r.get_subreddit(name)
            # This will cause an exception if it's a bad sub
            subreddit.title
        # If the sub doesn't exist reply with error
        except praw.errors.InvalidSubreddit:
            print("ERROR: Invalid Subreddit")
            logging.debug("ERROR: InvalidSub. Subname {} in ".format(name) +
                          "determine_sub_stats")

            return ['/r/' + name, 'inval']
        # If the sub is private let user know the bot can't look at it
        except praw.errors.Forbidden:
            print("ERROR: Forbidden")
            logging.debug("ERROR: Forbidden. Subname {} in ".format(name) +
                          "notify_sub")

            return ['/r/' + name, 'priv']
        # Catch any other weird errors and let the user know there was an error
        # And message the mods the error, and message link
        except praw.errors.APIException as e:
            print("ERROR: Praw API Exception {}".format(e))
            logging.debug("ERROR: Praw API Exception {}".format(e) +
                          " in determine_sub_stats")

            r.send_message('EDGYALLCAPSUSERNAME',
                           'Error with nomination',
                           ('There was an error with ' +
                            MESSAGE_LINK.format(str(mail.id)) + '\n\n' +
                            'Err was: ```\n\n{}\n```'.format(e)),
                           captcha=None)

            return ['/r/' + name, 'err']
        # Catch any other weird error so bot doesn't just crash
        # Message the mods and let user know
        except Exception as e:
            print("ERROR: {}".format(e))
            logging.debug("ERROR: Exception {}".format(e) +
                          " in determine_sub_stats")
            r.send_message('EDGYALLCAPSUSERNAME',
                           'Error with nomination',
                           ('There was an error with ' +
                            MESSAGE_LINK.format(str(mail.id)) + '\n\n' +
                            'Err was: ```\n\n{}\n```'.format(e)),
                           captcha=None)

            return ['/r/' + name, 'err']

        # Assume any subreddit over 10,000 subscribers was not meant
        # to be nominated and silently ignore it
        if subreddit.subscribers > 10000:
            return False

        # Calculate roughly how many months old the sub is
        # since reddit's api returns time since epoch in seconds
        months_old = calculate_months_old(subreddit)

        # If it is valid for nomination create a dictionary of info
        # and message user and return the dictionary
        if int(subreddit.subscribers) < 1000 and months_old >= 1:
            d = dict(subscriber_count=int(subreddit.subscribers),
                     sub='/r/' + name,
                     sidebar=subreddit.description,
                     months_old=months_old,
                     eighteen=subreddit.over18,
                     mail_link=MESSAGE_LINK.format(mail.id))

            return d
        # If it's too big let them know
        elif int(subreddit.subscribers) > 999:
            return ['/r/' + name, 'toobig']
        # If it's too new let them know
        else:
            return ['/r/' + name, 'toonew']


def make_nomination_post(subs, private_sub):
    nominations = []
    for sub in subs:
        title = sub['sub']
        nominations.append(title)
        body = NOM_POST.format(sub['sub'],
                               sub['subscriber_count'],
                               sub['months_old'],
                               sub['eighteen'],
                               sub['mail_link'],
                               sub['sidebar'])

        print("Submitting new nomination...")
        # Submit nomination and flair and approve it
        submission = private_sub.submit(title, text=body)
        submission.set_flair(flair_css_class="intern",
                             flair_text="NOMINATED")
        submission.approve()

    return nominations


def generate_nominations_reply(subs):
    if len(subs) == 1:
        return NOM_REPLY.format('nomination! ' + ' '.join(subs) + ' has')
    elif len(subs) == 2:
        return NOM_REPLY.format('nominations! ' + ' and '.join(subs) + ' have')
    elif len(subs) > 2:
        return NOM_REPLY.format('nominations! ' + ', '.join(subs) + ' have')
    else:
        return ''


def generate_errors_reply(errors):
    # this function will return a blank string for
    # concatenation if there are no errors present
    error_types = dict(inval=[],
                       priv=[],
                       err=[],
                       toobig=[],
                       toonew=[])
    error_message = ''

    # this will find all the errors and append them
    # to the appropriate list in the dictionary
    if len(errors) > 0:
        for error in errors:
            error_types[error[1]].append(error[0])
    else:
        return ''

    # then go through the dictionary and create all the
    # strings for the error messages
    for error, subs in error_types.items():
        if len(subs) > 0:
            if error == 'inval':
                error_message += INVAL_REPLY.format(
                    pluralize_error_message(subs)) + "\n\n"
            elif error == 'priv':
                error_message += PRIV_REPLY.format(
                    pluralize_error_message(subs)) + "\n\n"
            elif error == 'err':
                error_message += ERR_REPLY.format(
                    pluralize_error_message(subs)) + "\n\n"
            elif error == 'toobig':
                error_message += BIG_REPLY.format(
                    pluralize_error_message(subs)) + "\n\n"
            elif error == 'toonew':
                error_message += NEW_REPLY.format(
                    pluralize_error_message(subs)) + "\n\n"

    return error_message


def pluralize_error_message(subs):
    if len(subs) == 1:
        return ' '.join(subs) + ' could not be nominated because it is'
    elif len(subs) == 2:
        return ' and '.join(subs) + ' could not be nominated because they are'
    elif len(subs) > 2:
        return ', '.join(subs) + ' could not be nominated because they are '


def calculate_months_old(subreddit):
    # Average amount of days in a month
    d_per_m = 30.4166666667
    # Converts the subreddit creation date from time since epoch
    # to days and hours only the days are relevant
    a = timedelta(seconds=subreddit.created)
    # Converts the current time since epoch to days and hours
    b = timedelta(seconds=time.time())

    # Calculate roughly how many months old it is
    d = (b.days / d_per_m) - (a.days / d_per_m)
    # Use ceiling to give some leeway because of how
    # rough the calculation is
    return math.ceil(d)


def main():
    print("Logging in...")
    r = praw.Reddit('Tiny_Sub_Of_The_Day v1.5 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    db = dataset.connect('sqlite:///tsotd.db')
    public_sub = r.get_subreddit(PUBLIC_SUB)
    private_sub = r.get_subreddit(PRIVATE_SUB)

    while True:
        try:
            # refresh the token if neccessary
            o.refresh()
            find_posts_to_xpost(public_sub, private_sub, r, db)
            get_nominations(public_sub, private_sub, db, r)
        # Catches if reddit goes down
        except requests.exceptions.ConnectionError as e:
            print("ERROR: Reddit is down...")
            logging.debug("ERROR: Reddit went down")
            time.sleep(200)  # sleep because reddit is down
            # Then send the traceback so I know if I need
            # to take any action if reddit went down
            # while it was trying to post something
            # Get the traceback to send
            etype, value, tb = sys.exc_info()
            tb_s = '\n'.join(traceback.format_tb(tb, None))
            r.send_message('EDGYALLCAPSUSERNAME',
                           'Hey, I went down',
                           ('Here\'s why ```{}```\n\n'.format(e) +
                            "And here's the traceback:\n\n" +
                            "```\n\n{}\n```".format(tb_s)),
                           captcha=None)
        # And a catch all, messages me and continues functioning
        except Exception as e:
            print("ERROR: {}".format(e))
            logging.debug("ERROR: Exception {}".format(e) +
                          " in main exited")
            # Get the traceback to send
            etype, value, tb = sys.exc_info()
            tb_s = '\n'.join(traceback.format_tb(tb, None))
            print(tb_s)
            r.send_message('EDGYALLCAPSUSERNAME',
                           'Hey, I went down',
                           ('Here\'s why ```{}```\n\n'.format(e) +
                            "And here's the traceback:\n\n" +
                            "```\n\n{}\n```".format(tb_s)),
                           captcha=None)

            # Exit to make fix
            exit(1)

        # wait 5 minutes to not go over request limit
        print("Sleeping...")
        time.sleep(180)


if __name__ == '__main__':
    main()
